!!!VAZNO!!! potreban je eigen-3.2.6 folder !!!

Program je napravljen u KDevelopu.

== Kontrole ================================

pokreni/nastavi = "s"
zaustavi= "x"
izlaz = "esc"

============================================

Na pocetku programa napisane su sve globalne varijable.
Moguce je mijenjati mjesto kamere, prikaz linija po kojima se 
krece sfera(varijable "dcp" i "dbc"), broj poligona pri crtanju sfere.


============================================

Pri pokretanju programa sfera se nalazi uz gornju lijevu prozora. Pritiskom na tipku za pokretanje sfera se pocnije kretati po predefiniranim bezierovim linijama(linije mozete prikazati pomocu dvije bool varijable na pocetku programa). Sfera se prilikom kretanja i okrece oko z osi. Tijekom kretanja sfera ostavlja trag. Animacija zavrsava u doljnjem lijevom kutu ekrana.

============================================

Pomoc pri rjesavanju problema nasao sam na stranicama:
http://www.opengl-tutorial.org
http://www.stackoverflow.com
http://www.youtube.com
http://www.glprogramming.com