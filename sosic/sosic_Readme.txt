Zadatak.4
Sosic Ivona

Program prikazuje kako će se gibati model čelične kugle po Bezierovoj putanji. 

Za početak, u kod su uključene sve potrebne knjižice (library) kako bi nam kod ispravno radio. 
Definirana je konstanta PI, koja će mi koristiti u “drawSphere” funkciji za crtanje čelične kugle.

U funkciji za crtanje kugle (drawSphere), parametri x=0.0 I y=0.0 će biti x I y pozicija središta  iscrtanog kruga tj. Kugle.
Parametar radius nam određuje koliko “velika” će nam biti kugla. Za isrctavanje kugle, korištena je metoda crtanja pomoću trokuta
glBegin(GL_TRIANGLE_FAN), gdje je na početku definiran parametar triangleAmount. Triangle amount je broj trokutova koji je
potreban da nam iscrta obris kugle, što je veći broj kuteva kugla će izgledati zaglađenija. 
GL_TRIANGLE_FAN nam radi tako da se stvara stožac u našem volumenu gledanja, prvi “FAN” nam stvara oblik stošca,
koristeći prvi vertex kao točku tog stošca, dok ostali vrhovi stvaraju točke duž kružnice. Drugi “FAN” stvara krug koji se nalazi
na xy ravnini. GlVertex2f(x, y) će nam biti središnjica kugle.

Nadalje, za upravljanje programom korištene su tipke S (za pokretanje animacije), E (za pauziranje animacije), R (za ponovni
prikaz animacije) I ESC (za izlaz iz animacije). A, za upravljanje putanjom naše kugle koriste se UP I DOWN, LEFT I RIGHT tipke,
koje nam koriste za visinu I širinu naše putanje. Ovisno koju tipku pritisnemo, širina ili visina će nam se smanjiti ili povisiti
za 0.5 inkrementa. Najmanja visina ili širina bit će 0.5.

U funkciji za crtanje (drawScene), određena je boja pozadine kao crna, scena je smještena u frustum s glTranslatef(-15.0, -15.0, -25.0).
A putanju sam kugle sam definirala translacijom glTranslatef(h*t, v*t - (g/2.0)*t*t, 0.0). 
Parametar “h” će biti horizontalna komponenta početne brzine, a “v” vertikalna komponenta. 
“t” će nam biti vremenski parametar, a “g” kao neka težina tj. Gravitacija koja uječe na pad te kugle. 
Sve ove parametre sam ispisala prije izrađivanja funkcija. Kuglu sam ubacila funkcijom drawSphere te sam odredila sivu boju te kugle.

U drawScene sam također ubacila funkciju writeData, koja se nadovezuje uz funkcije writeBitmapString I floatToString.
Te funkcije sam koristila tako da nam u programu u gornjem lijevom kutu bijelim fontom piše trenutno stanje parametara “h” I “v”,
te da nam olakša upravljanje kuglom.

U setup funkciji, određena je ambijentalna I specular svjetlost, dodana je I shininess. 
Pilagođavane su vrijednosti dok nisu postale ugodne boje.

Prilikom pokretanja aplikacije u prozoru će se ispisati “Celicna kugla.”, prozor smo definirali s  glutInitWindowSize koji će biti
veličine 700x700.


Materijali koji su koristili prilikom izrade programa:
https://www.cse.msu.edu/~cse872/tutorial3.html
Open GL Super Biblija http://opengl.czweb.org/ewtoc.html
http://openglut.sourceforge.net/group__bitmapfont.html
http://stackoverflow.com/questions/8043923/gl-triangle-fan-explanation
http://www.glprogramming.com/red/chapter12.html
