Program generira casu i rotira je oko proizvoljne osi

Varijable:

	Dimenzije case sastoje se od vise dijelova: dno case (x_bottom..), visina drzaca case (x_height..), dimenzija vanjskog ruba case (x_bezierO.. -rub se generirao pomocu beziera, O predstavlja outline), dimenzije unutarnje ruba case (x_bezierI..) i vrha case (x_top..)

	Svim tockama z je pocetno postavljen na 0

	Vrijednosti kuteva rotacije: -angle - pocetno postavljen na 0, povećava se u displayu svaki put za 1, sluzi za rotiranje cijele case oko z osi
			     				 -rot_angleb - postavljen na 10, sluzi za izracun x i z koordinata kod kontrolnih tocaka bezierovih ploha
			     				 -rot_angle - postavljen na 30, sluzi za izracun x i z koordinata kod crtanja bezierovih ploha i ostatka case (dna i vrha), 3 puta je veci od rot_angleb kako se bezierove plohe nebi iscrtavale jedna preko druge(jer bezirova ploha ima 4 kontorlne tocke i napravi 3 rotacije) 

	Kontrolne tocke beziera: -2D su matrice 4x3, a tocke su definirane dimenzijama: x_bezier, y_bezier, z 
			 				 -3D su matrice 4x4x3, tocke su u skupinama - prva skupina su x_bezier, y_bezier, z; a sljedece 3 ovisne o rotaciji cije se dimenzije racunaju x' = x * cos(angle) i z' = x * sin(angle) (x' i z' varijable koje se pohranjuju kao kontrolna tocka, angle - kut roatcije(rot_angleb pomnozen 1,2 ili 3 ovisno koja skupina, svaka skupina za 1 vise pomnozena)  


Rjesevanje problema:

	Prvo sam napravila konturu case pomocu GL_LINE_STRIP i bezierovom krivuljom, te sam nju rotirala oko y osi s jako malim kutem - time se dobio oblik case, ali ne i model tj nije se moglo zicano prikazati casu (prikaz same konture je zadrzan)

	Casu samo morala razbiti na 3 dijela: dno i drzac, oblik(rubovi) i vrh

		Drzac je napravljen od 2 GL_TRIANGLE_FAN  koji su dno i kraj visine drzaca - pocetna tocka triangle_fan-a je (0,0,0), 2. je (x_bottom(ili height), y_bottom, z), a ostale se racunaju rotacijom kao sto se racunalo kontrolne tocke 3D beziera, te od 3 GL_TRIANGLE_STRIP koji su pratili oblik donjeg dijela case

		Oblik(rubovi) case napravljen je pomocu bezireovih ploha, koje su se iscrtavale dio po dio rotiranjem prostora s kutom rot_angle

		Vrh case napravljen je pomocu GL_TRIANGLE_STRIP 

	Prozirnost case dobiven naredbom glColor4f(0.5, 0.5, 0.5, 0.3) gdje zadnji clan predstavlja prozirnost modela (koristenje glColor4f omogucen je naredbama glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE) i glEnable(GL_COLOR_MATERIAL))


Koristenje porgrama:

	Pokrenuti program, casa se rotira oko z-osi i prikazan je cijeli model case

	Tipke: - s - prikaz cijelog modela case
	       - w - prikaz zicanog modela case
	       - o - prikaz samo jedne konture case
	       - ESC - izlaz iz programa

Reference:

	Stvaranje donjeg dijela case: http://stackoverflow.com/questions/4117084/opengl-how-to-lathe-a-2d-shape-into-3d
	Stvaranje bezierove plohe: http://www.glprogramming.com/red/chapter12.html
	Tipke, svjetla: vjezba v07
