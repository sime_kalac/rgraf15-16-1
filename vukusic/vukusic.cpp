#include <GL/gl.h>
#include <GL/glut.h>
#include <stdlib.h>
#include <cmath>
#include <iostream>
#include <Eigen/Dense>
using namespace Eigen;
using namespace std;

int kreni = 0, tocka; //kreni služi za pokretanje animacije, točka služi da znamo koju točku pomičemo
Vector4d pt_x, pt_y, pt_z; // vektor s vrijednostima kontrolnih točaka
VectorXd t_x, t_y, t_z, ts; //vektor s točkama koje nam služe za iscrtavanje bezierove krivulje
float dist, angle = -90, t=25;
bool strana=false, kraj=false;

void handleKeypress(unsigned char key, int /*x*/, int /*y*/) {   //funkcija obrađivanja pritiska tipki
switch (key) {
    case 27:
        exit(0); 
	    break;
	
	case 's': {
	    kreni = 1;
	    if(kraj) { kraj=false; }
	}
	    break;
	case 'e': 
	    kreni = 0;
	    break;
	
	case '1':
	    tocka = 0;
	    break;
	case '2':
	    tocka = 1;
	    break;
	case '3':
	    tocka = 2;
	    break;
	case '4':
	    tocka = 3;
	    break;
	    
	case 'h': 
	    pt_x(tocka)+= 0.1;
	    break;
	case 'f': 
	    pt_x(tocka)-= 0.1;
	    break;    
	case 't': 
	    pt_y(tocka)+= 0.1;
	    break;
	case 'g': 
	    pt_y(tocka) -= 0.1;	 
	    break;
	
	case 'u': 
	    pt_z(tocka) += 0.1;
	    break;
	    
	case 'j': 
	    pt_z(tocka) -= 0.1;    
	    break;

	case 'i': 
	    if (strana==true) strana=false;
	    else strana=true;
	    break;  
    }
}
long binomials ( long n, long k ) {
    long i;
    long b;
    if ( 0 == k || n == k )
    {
        return 1;
    }
    if ( k > n )
    {
        return 0;
    }
    if ( k > ( n - k ) )
    {
        k = n - k;
    }
    if ( 1 == k )
    {
        return n;
    }
    b = 1;
    for ( i = 1; i <= k; ++i )
    {
        b *= ( n - ( k - i ) );
        if ( b < 0 ) return -1; /* ERR!!!! OVERFLOW */
        b /= i;
    }
    return b;
}
double polyterm ( const int &n, const int &k, const double &t )
{
    return pow ( ( 1.-t ),n-k ) *pow ( t,k );
}
double getValue ( const double &t, const VectorXd &v )
{
    int order = v.size()-1;
    double value = 0;
    for ( int n=order, k=0; k<=n; k++ )
    {
        if ( v ( k ) ==0 ) continue;
        value += binomials ( n,k ) * polyterm ( n,k,t ) * v ( k );
    }
    return value;
}
void changeSize ( int w, int h )
{
    if ( h == 0 )
    {
        h = 1;
    }
    float ratio = 1.0* w / h;
    glMatrixMode ( GL_PROJECTION );
    glLoadIdentity();
    glViewport ( 0, 0, w, h );
    gluPerspective ( 45,ratio,1,1000 ); // view angle u y, aspect, near, far
}
void drawControlPoints() //crtanje kontrolnih točaka
{
    glPushMatrix();
    glColor3f ( 1.0f, 1.0f, 1.0f );
    glBegin ( GL_LINE_STRIP ); //točke će bit spojene ravnim linijama
    glVertex3f ( pt_x ( 0 ), pt_y ( 0 ), pt_z ( 0 ) );
    glVertex3f ( pt_x ( 1 ), pt_y ( 1 ), pt_z ( 1 ) );
    glVertex3f ( pt_x ( 2 ), pt_y ( 2 ), pt_z ( 2 ) );
    glVertex3f ( pt_x ( 3 ), pt_y ( 3 ), pt_z ( 3 ) );
    glEnd();
    glPopMatrix();
}
int velicina;  
void generateBezierCurve()
{
    ts = VectorXd::LinSpaced ( 100,0,1. );
    t_x.resize ( ts.size() );
    t_y.resize ( ts.size() );
    t_z.resize ( ts.size() );
    
    int idx;
    
    for (idx=0; idx< ts.size(); ++idx )
    {
        t_x ( idx ) = getValue ( ts ( idx ), pt_x );
        t_y ( idx ) = getValue ( ts ( idx ), pt_y );
        t_z ( idx ) = getValue ( ts ( idx ), pt_z );
    }
    
    velicina=idx-1;
    
}
void drawBezierCurve() //crtanje bezierove krivulje
{
    glPushMatrix();
    glColor3f ( 1.0f, 0.0f, 0.0f );
    glBegin ( GL_LINE_STRIP );
    for ( int i=0; i< ts.size(); ++i ) glVertex3f ( t_x ( i ), t_y ( i ), t_z ( i ) ); 
    glEnd();
    glPopMatrix();
}
void drawSphere(double /*r*/, int lats, int longs) { //crtanje sfere
	float x,y,z;

	if (!strana){ //ako ide s lijeva na desno
		x = t_x(dist*100); 
		y = t_y(dist*100); 
		z = t_z(dist*100);
    } else { //ako ide s desna na lijevo
		x = t_x (velicina -(dist*100));
		y = t_y (velicina -(dist*100));
		z = t_z (velicina -(dist*100));
    }
	glPushMatrix();
	glTranslatef(x,y,z);
	glScalef(0.5, 0.5, 0.5);
	int i, j;  
       
	for(i = 0; i <= lats; i++) {
		double lat0 = M_PI * (-0.5 + (double) (i - 1) / lats);
		double z0  = sin(lat0);
		double zr0 =  cos(lat0);

		glBegin(GL_QUAD_STRIP);
		for(j = 0; j <= longs; j++) {
			double lng = 2 * M_PI * (double) (j - 1) / longs;
			double x = cos(lng);
			double lat1 = M_PI * (-0.5 + (double) i / lats);
			double z1 = sin(lat1);
			double zr1 = cos(lat1);

			double y = sin(lng);

			glNormal3f(x * zr0, y * zr0, z0);
			glVertex3f(x * zr0, y * zr0, z0);
			glNormal3f(x * zr1, y * zr1, z1);
			glVertex3f(x * zr1, y * zr1, z1);
	}
	glEnd();
	}
	glPopMatrix();
} 
void scena()
{
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  
    //boja pozadine
    glClearColor (0., 0., 0., 0.0);
    
    glMatrixMode ( GL_MODELVIEW ); 
    glLoadIdentity(); 
    gluLookAt ( 0.0,0.0,10.0,0.0,0.0,-1.0,0.0f,1.0f,0.0f ); 
    
	//pozivanje funkcija za crtanje objekata i potrebne transformacije
	glPushMatrix();
	glRotatef(-90, 0,1,0);
		glPushMatrix();
		glRotatef(90, 0,1,0);
		glTranslatef(0.95,0.5,0);
		drawControlPoints();
		drawBezierCurve(); 
			glPushMatrix();   
			glColor3f(0.2f, 0.2f, 0.2f);
			drawSphere(1, 25, 25);
			glPopMatrix();
		glPopMatrix();
	glPopMatrix();
    
    glutSwapBuffers();
}
void update ( int /*value*/ )                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
{
    glutPostRedisplay();
    generateBezierCurve();
    drawBezierCurve();
	if (dist<0.99){ //ako kugla nije došla do kraja
		if (kreni==1) dist = dist + 0.01; //ako je animacija pomiči kuglu
	} else{
		 kraj=true;
		 kreni=0; //zaustavi animaciju
		 dist=0;
		if (strana==true) strana=false; //mijenjamo varijablu da znamo na kojoj stani je kugla
		else strana=true;
    }
    glutTimerFunc ( t, update, 0 );
}

int main ( int argc, char **argv )
{
    //kontrolne točke za bezierovu krivulju
    pt_x << -0.95, -0.25, 0.6, 0.95;
    pt_y << -0.5, 0.8, 0.6, -0.5;
    pt_z << 0, 0, 0, 0;
    dist = 0.;

    generateBezierCurve();
    glutInit ( &argc, argv );
    
    glutInitDisplayMode (GLUT_DOUBLE| GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize (1000, 700);
    
    glutCreateWindow ( "Moving a sphere along Bezier spline" );
    glutReshapeFunc ( changeSize );
    glutDisplayFunc ( scena );
    glutKeyboardFunc(handleKeypress);
    glutTimerFunc ( 25, update, 0 );
    glShadeModel (GL_SMOOTH);
    
    //osvjetljenje i sjenčanje
	const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
	const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
	const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	const GLfloat light_position[] = { 2.0f, 2.0f, 5.0f, 0.0f };
	const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
	const GLfloat mat_diffuse[]    = { 0.f, 0.f, 0.f, 1.0f };
	const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
	const GLfloat high_shininess[] = { 100.0f };
    
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);
    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);
    
    glutMainLoop();
    return 0;
}