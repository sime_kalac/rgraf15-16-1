Kod je pisan u Kdevelop programu. 

Program radi tako da se tipkom "w" uključuje žičani model, tipkom "s" se prikazuje cijela čaša, a tipkom esc se izlazi iz programa.
Za prikaz čaše korišteni su trokuti i pravokutnici, a za žičani model čaše, linije.
Čaša tijekom vrtnje bude osvjetljenja i u sjeni.

Pomoć pri pisanju koda bile su mi vježbe s kolegija, tutorijali s interneta i opengl stranice.

