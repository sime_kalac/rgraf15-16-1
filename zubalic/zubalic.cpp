#include <GL/gl.h>
#include <GL/glut.h>
#include <stdlib.h>
#include <cmath>

#include <iostream>
#include <Eigen/Dense>
using namespace Eigen;
using namespace std;

int kreni = 0;
int tocka = 1;
int okreni = 0;
float dist = 0.0;
GLfloat angle = 0.0;

Vector4d pt_x, pt_y, pt_z;
long binomials ( long n, long k )
{
    // n && k >=0
    long i;
    long b;

    if ( 0 == k || n == k )
    {
        return 1;
    }

    if ( k > n )
    {
        return 0;
    }

    if ( k > ( n - k ) )
    {
        k = n - k;
    }

    if ( 1 == k )
    {
        return n;
    }

    b = 1;

    for ( i = 1; i <= k; ++i )
    {
        b *= ( n - ( k - i ) );

        if ( b < 0 ) return -1; /* ERR!!!! OVERFLOW */

        b /= i;
    }

    return b;
}

double polyterm ( const int &n, const int &k, const double &t )
{
    return pow ( ( 1.-t ),n-k ) *pow ( t,k );
}

double getValue ( const double &t, const VectorXd &v )
{
    int order = v.size()-1;
    double value = 0;

    for ( int n=order, k=0; k<=n; k++ )
    {
        if ( v ( k ) ==0 ) continue;

        value += binomials ( n,k ) * polyterm ( n,k,t ) * v ( k );
    }

    return value;
}

void changeSize ( int w, int h )
{
    if ( h == 0 )
    {
        h = 1;
    }

    float ratio = 1.0* w / h;

    glMatrixMode ( GL_PROJECTION );
    glLoadIdentity();
    glViewport ( 0, 0, w, h );
    gluPerspective ( 45,ratio,1,1000 ); // view angle u y, aspect, near, far
}


VectorXd t_x, t_y, t_z;
VectorXd ts;
void drawControlPoints()
{
    glPushMatrix();
    glColor3f ( 1.0f, 1.0f, 1.0f );
    glBegin ( GL_LINE_STRIP );
    glVertex3f ( pt_x ( 0 ), pt_y ( 0 ), pt_z ( 0 ) );
    glVertex3f ( pt_x ( 1 ), pt_y ( 1 ), pt_z ( 1 ) );
    glVertex3f ( pt_x ( 2 ), pt_y ( 2 ), pt_z ( 2 ) );
    glVertex3f ( pt_x ( 3 ), pt_y ( 3 ), pt_z ( 3 ) );
    glEnd();
    glPopMatrix();
}

void generateBezierCurve()
{
    ts = VectorXd::LinSpaced ( 21,0,1. );
    t_x.resize ( ts.size() );
    t_y.resize ( ts.size() );
    t_z.resize ( ts.size() );

    for ( int idx=0; idx< ts.size(); ++idx )
    {
        t_x ( idx ) = getValue ( ts ( idx ), pt_x );
        t_y ( idx ) = getValue ( ts ( idx ), pt_y );
        t_z ( idx ) = getValue ( ts ( idx ), pt_z );
    }
}

void drawBezierCurve()
{
    glPushMatrix();
    glColor3f ( 1.0f, 1.0f, 1.0f );
    glBegin ( GL_LINE_STRIP );
    for ( int i=0; i< ts.size(); ++i )
    { glVertex3f ( t_x ( i ), t_y ( i ), t_z ( i ) ); }
    glEnd();
    glPopMatrix();
}


void drawSphere(double /*radius*/ , int horizontal_lines, int vertical_lines) {
  float x,y,z;
    x = getValue(dist, pt_x);
    y = getValue(dist, pt_y);
    z = getValue(dist, pt_z);
      glPushMatrix();
     
      glTranslatef(x,y,z);
      glScalef(0.5, 0.5, 0.5);
       int i, j;
       
       for(i = 0; i <= horizontal_lines; i++) {
           double lat0 = M_PI * (-0.5 + (double) (i - 1) / horizontal_lines);
           double z0  = sin(lat0);
           double zr0 =  cos(lat0);
    
           double lat1 = M_PI * (-0.5 + (double) i / horizontal_lines);
           double z1 = sin(lat1);
           double zr1 = cos(lat1);
   
           glBegin(GL_QUAD_STRIP);
           for(j = 0; j <= vertical_lines; j++) {
               double lng = 2 * M_PI * (double) (j - 1) / vertical_lines;
               double x = cos(lng);
               double y = sin(lng);
    
               glNormal3f(x * zr0, y * zr0, z0);
               glVertex3f(x * zr0, y * zr0, z0);
               glNormal3f(x * zr1, y * zr1, z1);
               glVertex3f(x * zr1, y * zr1, z1);
           }
           glEnd();
       }
       
         glPopMatrix();
     
       
   }
   
GLfloat whiteSpecularLight[] = {1.0, 1.0, 1.0}; 
GLfloat blackAmbientLight[] = {0.0, 0.0, 0.0}; 
GLfloat whiteDiffuseLight[] = {1.0, 1.0, 1.0};
GLfloat blankMaterial[] = {0.0, 0.0, 0.0};
GLfloat mShininess[] = {128};

bool diffuse = false;
bool emissive = false;
bool specular = false;
bool enable_light = false;

void init (void) {
    glEnable (GL_DEPTH_TEST);
    if (enable_light == true) glEnable (GL_LIGHTING);
    else glDisable(GL_LIGHTING);
    glEnable (GL_LIGHT0);
}

void light (void) {
    glLightfv(GL_LIGHT0, GL_SPECULAR, whiteSpecularLight);
    glLightfv(GL_LIGHT0, GL_AMBIENT, blackAmbientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDiffuseLight);
}


void drawScene()
{
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glClearColor (0.3, 0.3, 0.3, 0.0);
    
    glMatrixMode ( GL_MODELVIEW );
    glLoadIdentity();
    gluLookAt ( 0.0,0.0,5.0, 
                1.0,0.0,-1.0, 
                0.0f,1.0f,0.0f ); 

    glPushMatrix();
       glRotatef(angle, 0,1,0);
       
	  glPushMatrix();
	  glRotatef(360, 0,1,0);
	  glTranslatef(0.95,0.5,0);
	  
            glPushMatrix();
	      glPushMatrix();
	      glColor3f(0.5, 0.5, 0.5);
		drawSphere(1, 25, 25);
	      glPopMatrix();
	      
	    glPopMatrix();
            if(kreni==0) drawControlPoints();
            drawBezierCurve();
	  glPopMatrix();
	
    glPopMatrix();
   
    glutSwapBuffers();
}

float t=25;
int loop=1;
float pomak = 0.01;

void update ( int /*value*/ )
{
    glutPostRedisplay();
   
    generateBezierCurve();
    drawBezierCurve();
    drawSphere(1, 25, 25);
    
   light();
   init();
     
    if (dist<1) {
       if (kreni ==1) {
	 dist = dist + pomak;
	 enable_light = true;
	 init();
       }
       
      glutTimerFunc ( 25, update, 0 );
    }
    else {
        enable_light = false;
        init();
    }

    if (okreni == 1) {
      angle += 25;
      okreni = 0;
    }
   
    if(angle > 360 ){angle = 0;}
   
}

void handleKeypress(unsigned char key,
                    int, int) {    
    switch (key) {
        case 27: //Escape key
            exit(0); //Exit the program
	    break;
	case 's': //stop
	    kreni = 1;
	    break;
	
	case 'e': //end
	    kreni = 0;
	    break;
	
	case 49: //prva (odnosno "nulta") tocka se ne moze micati
	    break;
	    
	case 50:
	    tocka = 1;
	    break;
	
	case 51:
	    tocka = 2;
	    break;
	
	case 52:
	    tocka = 3;
	    break;
	
	case 'r': //right 
	    pt_x(tocka) = pt_x(tocka) + 0.1;
	    break;
	
	case 'l': //left
	    pt_x(tocka) = pt_x(tocka) - 0.1;
	    break;
	    
	case 'u': //up
	    pt_y(tocka) = pt_y(tocka) + 0.1;
	    break;
	    
	case 'd': //down
	    pt_y(tocka) = pt_y(tocka) - 0.1;	  
	    break;
	    
	case 'a': //rotiraj
	    okreni = 1;
	  break;
	  
	case '0':
	  glutTimerFunc ( 25, update, 0 );
	  dist = 0;
	  kreni = 0;
	  enable_light = false;
	  if (pomak <= 0.08) pomak = pomak + 0.02;
	  init();
	  break;
	    
    }

}

int main ( int argc, char **argv )
{  
    pt_x << -0.95, -0.25, 0.6, 0.95;
    pt_y << -0.5, 0.8, 0.6, -0.5;
    pt_z << 0, 0, 0, 0;
    dist = 0.;
    
    generateBezierCurve();
    glutInit ( &argc, argv );
    glutInitDisplayMode ( GLUT_DOUBLE /*| GLUT_RGB | GLUT_DEPTH */);
    glutInitWindowSize ( 500, 500 );
    glutCreateWindow ( "Lansiranje kugle po krivulji" ); 
    glutReshapeFunc ( changeSize );
    glutDisplayFunc ( drawScene );
    glutKeyboardFunc(handleKeypress);
    glutTimerFunc ( 25, update, 0 );
    glutMainLoop();

    return 0;
}