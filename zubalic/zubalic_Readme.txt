LANSIRANJE KUGLE PO BEZIEROVOJ KRIVULJI
1.) Crtanje kontrolnih točaka  i crtanje Bezierove krivulje - korišten template s vježbi
-> prilagođeno ovom zadatku tako što sam ovisno o pritisku na određenu tipku mijenjala vrijednost kontrolnih točaka
Prva točka se ne može mijenjati, a ostale točke se odabiru pristiskom na tipku 2, 3 ili 4, nakon čega se mogu i mijenjati (default: 2.točka).
Pritiskom na tipku 'r' odabrana točka ide u desno (right), pritiskom na tipku 'l' točka ide u lijevo (left),
pritisak 'u' tipke rezultira pomakom točke prema gore (up) i pritiskom na 'd' točka se spušta prema dolje (down). 
Sve prethodno navedene operacije se izvode s pomakom od 0.1. 
Pritisak tipke 'a' rezultira rotacijom Bezierove krivulje po kojoj se lansira kugla za 25 stupnjeva.

2.) Crtanje kugle
link koji sam koristila - http://www.cburch.com/cs/490/sched/feb8/ i minimalno izmjenila varijable.
također sam proučila ovaj link - http://stackoverflow.com/questions/7687148/drawing-sphere-in-opengl-without-using-glusphere

3.) Lansiranje kugle
-korištenje varijable dist koja označava udaljenost kugle od kraja Bezierove krivulje. 
Dist je vrijednost između 0 i 1 i kada dođe do 1, znamo da je kugla došla do kraja krivulje.
Tu sam varijablu koristila unutar funkcije update koja se ponovo poziva svakih 25 milisekundi.
Početni pomak kugle je za 0.01 i lansiranje kugle se pokreće pritiskom na tipku 's' (start). 
Dok kugla "putuje", ne prikazuju se kontrolne točke, već samo krivulja.

4.) Lightening
- koristila sam template s vježbi s tim da sam se malo poigrala s tim na način da sam palila i gasila svjetlo ovisno o tome je li kugla "završila svoj put" ili "još putuje".
Pri samom pokretanju programa, svjetlo je ugašeno. Kada pretisnemo 's' (start), svjetlo se upali i kugla kreće na put. Kada dođe do kraja svjetlo se ponovo ugasi, dok se ponovo ne pokrene.

5.) Zaustavljanje kugle
Pritiskom tipke 'e' (end) kugla se zaustavlja na trenutnom položaju i ponovno se prikazuju kontrolne točke koje je ponovo moguće mijenjati.

6.) Završetak animacije ( i mogućnost ponovnog pokretanja)
Kada kugla dođe do kraja krivulje, svjetlo se ugasi i pristikom na '0' možemo ponovo postaviti kuglu na početnu poziciju te pritiskom na 's' ponovo pokrenuti animaciju. 
U sljedećih par pokretanja, razlika je u tome što je pri svakom novom pokretanju kugla brža, odnosno pomak veći.