Opis funkcija:
	getValue
		--> računa Bezierov splajn uz pomoć binominals (za binomni član) i polyterm (za polinomni član)
	changeSize
		--> određuje viewport i volumen pogleda
	init
		--> omogući  svjetlost i odredi da materijal utječe na boju
	light
		--> dodaj ambijentalnu, difuznu i zrcalnu komponentu
	keyboard
		--> omogućuje korisniku kontrolu putem Esc, x i s tipki
		--> (void)x, (void)y služe samo kako se ne bi nepotrebno ispisivale poruke upozorenja o nekorištenju parametra funkcije
	generateBezierCurve
		--> na temelju linearno raspoređene 21 točke u intervalu [0,1] i zadanih kontrolnih točki pt_x, pt_y i pt_z u main() kreira Bezierov splajn
	drawBezierCurve
		--> crta crveni trag, odnosno Bezierov splajn od početka do counter-a koji se inkrementira ovisno o trenutnom položaju lopte
	midPoint
		--> nađi polovište dužine AB i normaliziraj rezultat
	subdivide
		--> rekurzivno dijeli svaki trokut ABC dok dubina ne postane 3
	drawSphere
		--> crta loptu na temelju poligona s 20 strana (icosahaedron) čije se strane onda još podijele (subdivide) sve dok ne dobijemo
			dovoljno dobru aproksimaciju površine lopte
		--> ico_vert je polje sa vrhovima icosahaedrona
		--> brojevi X i Z su uzeti ovakvi kako bi udaljenost svakog vrha od središta bila 1
		--> ico_index sadrži skupove od 3 broja koji su indexi vrhova iz polja ico_vert koji će činiti trokut
		--> loptu translatiramo na njenu trenutnu poziciju određenu sphere_coo koji ovisi o Bezierovom splajnu
		--> obojamo loptu u plavo u skaliramo ju da bude manja 
	drawScene
		--> postavlja kameru i osvjetljenje te započinje s crtanjem Bezierovih krivulja i lopte
		--> sve translatiramo za (-5.4, 3.9, 0) kako bi početak bio u gornjem lijevom kutu (dobiveno testiranjem)
		--> glEnable (GL_LIGHTING) se smije odnositi samo na loptu, odnosno odmah nakon je glDisable(GL_LIGHTING) jer je u protivnom
			Bezierov splajn nevidljiv (svjetlost utječe na izračun boje točki pa postanu crne i ne vide se)
	update	
		--> svakih 25ms promijeni stanje, tj. pomakni loptu
		--> svaka Bezierova krivulja ide za angle od -90 do 0
		--> kada lopta prođe cijeli prvi splajn (angle1==0) postavi angle2 na -90 što označava početak kretanja po njemu (isto i za treći onda)
		--> counter se povećava u svakom koraku za jedan mali iznos (0.238) koji je testiranjem utvrđen da dobro prati brzinu kretanja lopte
	


Opis programa:
	U trenutku kada se program pokrene, u gornjem lijevom kutu prozora nalazi se plava lopta u stanju mirovanja. 
	Kada korisnik pritisne tipku 's' lopta počne padati odnosno kretati se po Bezierovoj krivulji i za sobom ostavlja crveni trag. 
	Animacija traje dok lopta ne stigne do donjeg desnog kuta prozora. 
	Korisnik može u bilo kojem trenutku pritisnuti 'x' i time zaustaviti animaciju, a u slučaju da želi nastaviti s animacijom samo ponovno pritisne 's'.
	U bilo kojem trenutku može se pritisnuti 'Esc' tipka čime se program zaustavlja u potpunosti i gasi. 
	

Tipke za kontrolu:
	Esc - u bilo kojem trenutku zatvara prozor programa
	S   - pokreće (ili nastavlja) program
	X   - zaustavlja (pauzira) program

Reference:
	1. Kodovi s vježbi kolegija Računalna grafika 
	2. OpenGL 2.1 Reference Pages (https://www.opengl.org/sdk/docs/man2/xhtml/)
	3. Some hints for building polygonal approximations of surfaces (http://opengl.org.ru/docs/pg/0208.html)
	