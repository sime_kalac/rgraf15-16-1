Opis

Pocetne vrijednosti u main funkciji nisam nikakvim tocno odredenim izborom napravila. x_t1 itd. sam napravila globalnima 
kako bih mogla mijenjati vrijednosti preko SpecialInput funkcije. Takoder sam preko varijable t mijenjala vrijednosti 
koordinata bezierovih koordinata, te u skladu s time mi se mijenjaju koordinate sfere, tj. kugle. Koristeci case_e 
boolean globalnu varijablu sam koristila u update funkciji te u keyboard funkciji da mi se ovisno o varijabli angle 
pokrece animacija. Vecinu sam koda koristila koje smo radili na laboratorijskim vjezbama, pretezito sam vukla iz 
v05_template-a. Funkcije za osjencavanje nisam u potpunosti sama napravila nego sam vecinu koda pronasla koristeci 
slijedeci izvor: http://www.programming-techniques.com/2012/05/rendering-spheres-glutsolidsphere-and.html.

Nacin na koji sam dosla do rjesenja jes metoda pokusaja i pogresaka. Pokusava sam tocno razumijeti na koji nacin dio 
koda funkcionira te time prilagoditi, odnosno rjesiti zadatak.

Osim navedenoga, ostale reference koje sam koristila jest preko stackoverflow-a, gamedev-a, 
http://www.swiftless.com/tutorials/opengl/keyboard.html, www.opengl.org diskusije.

Library koji sam jos koristila jest onaj koji je koristen u v05_template, tj eigen-3.2.6. te samo u direktoriju kojem 
je smjesten CMakeLists, p1.cpp kod i eigen-3.2.6 library u terminal ubaciti cmake .
Poslije kompalacije u terminal ubaciti make radi kompilacije pri cemu ce se napraviti izvrsna datoteka. Animaciju 
pokrenuti naredbom: ./p1

Animacija se koristi na sljedeci nacin: pritiskom tipke s pokrece se animacija. Tipkom e pauzira se animacija. 
Pomocu left i right tipki bezierove koordinate splina se pokrecu prema gore/dolje. Esc-om gasi se animacija.