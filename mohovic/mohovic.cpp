#include <iostream>
#include <GL/gl.h>
#include <GL/glut.h>
#include <array>
#include <Eigen/Dense>

using namespace std;

float angleX_1=0.0f;
float angleY_1=0.0f;
float angleZ_1=0.0f;

float angleX_2=0.0f;
float angleY_2=0.0f;
float angleZ_2=0.0f;

float radius=0.03f;

bool start_animation;
bool rotateX_1;
bool rotateY_1;
bool rotateZ_1;

bool rotateX_2;
bool rotateY_2;
bool rotateZ_2;

const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_position[] = { 1.0f, 1.0f, 4.0f, 0.0f };

const GLfloat mat_ambient[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.f, 0.f, 0.f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 100.0f };

void printInst()
{
    cout << "s - pokrece animaciju" << endl << "x -zaustavlja animaciju" << endl;
    cout << "q, w, e - rotacijska os gornjeg klatna" << endl;
    cout << "b, n, m - rotacijska os donjeg klatna" << endl;
    cout << "Esc - izlaz" << endl;
}

void initLight(){
    glEnable(GL_LIGHT0);   //ukljucuje svjetlinu
    glEnable(GL_NORMALIZE);  //ukljucuje normalizaciju vektora normala
    glEnable(GL_COLOR_MATERIAL); // ukljucuje boja materijala
    glEnable(GL_LIGHTING);      //ukljucuje osvjetljenje

                //svjetlost
    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);
    
    glShadeModel(GL_SMOOTH); //Enable smooth shading 
  
}


void update(int)
{

  
  if (start_animation){
      
      
    if(rotateX_1){
	if(angleX_1 < 90)angleX_1 +=2.0f;    // ogranicavanje rotacije zbog nepomicnog tjela nakon sto klatno dode do krajnje tocke rotira se oko iste osi u suprotnom smjeru
	if(angleX_1 == 90)angleX_1+=360;
	if(angleX_1 > 90)angleX_1-=2.0f;
	if(angleX_1==270)angleX_1=-90;
      }
      if(rotateY_1){
	  angleY_1+=2.0f;
	  if(angleY_1>360) angleY_1 -= 360;
      }
      if(rotateZ_1){
	if(angleZ_1 < 90)angleZ_1 +=2.0f;    // ogranicavanje rotacije zbog nepomicnog tjela nakon sto klatno dode do krajnje tocke rotira se oko iste osi u suprotnom smjeru
	if(angleZ_1 == 90)angleZ_1+=360;
	if(angleZ_1 > 90)angleZ_1-=2.0f;
	if(angleZ_1==270)angleZ_1=-90;
	  
	
	
      }
      
      if(rotateX_2){      
	angleX_2 +=2.0f;
	if(angleX_2>360) angleX_2 -= 360;
	
      }         
      if(rotateY_2){
	angleY_2 +=2.0f;
	if(angleY_2>360) angleY_2 -= 360;
	
      }
      if(rotateZ_2){
	angleZ_2 +=2.0f;
	if(angleZ_2>360) angleZ_2 -= 360;
	
      }
  }
  
  glutPostRedisplay();
   
  glutTimerFunc(25, update, 0);  //update screena za 25 ms
}  
    



void KeyPressed (unsigned char key, int , int){
    
  switch(key){         // konfiguracija tipki
    
    case 27:{ //ESC
      exit(0);
    }
    case 's':{
      start_animation=true;    
      break;
    }
    case 'x':{
      start_animation=false;
      break;
    }
    
    case'q':{
      rotateX_1=true;
      rotateY_1=false;
      rotateZ_1=false; 
      break;
    }
    case'w':{
      rotateZ_1=false;
      rotateX_1=false;
      rotateY_1=true;
      break;
    }   
    case'e':{
      rotateZ_1=true;
      rotateX_1=false;
      rotateY_1=false;
      break;
    }
    
    case 'b':{
      rotateZ_2=false;
      rotateX_2=true;
      rotateY_2=false;
      break;
      
    }     
    case 'n':{
      rotateZ_2=false;
      rotateX_2=false;
      rotateY_2=true;
      break;
      
   }  
   case 'm':{
      rotateZ_2=true;
      rotateX_2=false;
      rotateY_2=false;
      break;
   }
         
  } 
}

vector<float> x_path;
vector<float> y_path;
vector<float> z_path; 

int trail=0;

void createTrail(float x, float y, float z) {
   
 
    
    x_path.push_back (x);
    y_path.push_back (y);
    z_path.push_back (z);
   
    // iscrtavanje putanje
     glBegin(GL_LINE_STRIP);   
    glColor3f(0.5f, 0.f, 0.f);
    for(int j = 0; j < trail; j++){
        glVertex3f(x_path[j], y_path[j], z_path[j]);
    }
    glEnd();
} 


void drawScene()
{
  GLUquadricObj *quadobj;
  quadobj = gluNewQuadric();
  
  // ambient light
  GLfloat ambientColor[] = {1.0f, 1.f, 1.f, 1.0f}; 
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
  glLoadIdentity(); 
 
  glPushMatrix();         // pocetak hijerarhijskog modela rotacije klatna
    
    glPushMatrix();
      glTranslatef(0.f, 0.30f,-0.35f);
      glBegin(GL_POLYGON);               // nepomicna fiksna kocka
	glColor3f(0.8f, 0.8f, 0.8f);
	glVertex3f( -0.07, -0.07, -0.07);       // P1
	glVertex3f( -0.07,  0.07, -0.07);       // P2
	glVertex3f(  0.07,  0.07, -0.07);       // P3
	glVertex3f(  0.07, -0.07, -0.07);       // P4
      
      glEnd();
    glPopMatrix();
    glPushMatrix();
      glTranslatef(0.8f, -0.3f, 0.0f);  
     
      	//rotacija plavog klatna oko x osi
      glTranslatef(-0.8f, 0.5f,0.f);
      glRotatef(angleX_1, 1.f, 0.f, 0.f );  
      glTranslatef(0.8f, -0.5f,0.f);
      
      //rotacija plavog klatna oko y osi
      glTranslatef(-0.8f, 0.5f,0.f);
      glRotatef(angleY_1, 0.f, 1.f, 0.f );  
      glTranslatef(0.8f, -0.5f,0.f);
	
      
      //rotacija plavog klatna oko z osi
      glTranslatef(-0.8f, 0.5f, 0.f);
      glRotatef(angleZ_1, 0.f, 0.f, 1.f );  
      glTranslatef(0.8f, -0.5f,0.f);
	
	

	//prva kuglica
      glColor3f(0.0f, 1.0f, 0.0f);	
      glPushMatrix();
	glTranslatef(-0.8f, 0.5f,0.f);
	glutSolidSphere(radius, 100,100);
      glPopMatrix(); 
	    
	// plavo klatno
      glColor3f(0.0f, 0.0f, 1.0f);      
      glPushMatrix();
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.f, -0.8f,-0.48f);
	gluCylinder(quadobj,0.03f,0.03f,0.46f,100,100);
      glPopMatrix();
	
      glPushMatrix();
	
	 //rotacija crvenog klatna oko x osi
	glTranslatef(-0.8f, 0.f,0.f);
	glRotatef(angleX_2, 1.f, 0.f, 0.f );  
	glTranslatef(0.8f, 0.f,0.f);
            
      	  //rotacija crvenog klatna oko y osi
	glTranslatef(-0.8f, 0.f,0.f);
	glRotatef(angleY_2, 0.f, 1.f, 0.f );  
	glTranslatef(0.8f, 0.f,0.f);
	
	//rotacija crvenog klatna oko z osi
	glTranslatef(-0.8f, 0.f,0.f);
	glRotatef(angleZ_2, 0.f, 0.f, 1.f );  
	glTranslatef(0.8f, 0.f,0.f);
		
	  //druga kuglica
	glColor3f(0.0f, 1.0f, 0.0f);
	glPushMatrix();
	  glTranslatef(-0.8f,0.f,0.f);
	  glutSolidSphere(radius, 100,100);
	glPopMatrix(); 
	  
	  //crveno klatno
	glColor3f(1.0f, 0.0f, 0.0f);
	glPushMatrix();
	  glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	  glTranslatef(0.f, 0.f,-0.78f);
	  gluCylinder(quadobj,0.03f,0.03f,0.46f,100,100);
	glPopMatrix();
	  
	  // zadnja kuglica
	glColor3f(0.9f, 0.7f, 0.0f);
	glPushMatrix();
	  glTranslatef(-0.311f,0.f,0.f);
	  glScalef(0.5f, 1.0f,  1.0f);
	  glPushMatrix();
	    glutSolidSphere(radius, 100,100);
	    float mvMatrix[16];
	    glGetFloatv(GL_MODELVIEW_MATRIX, mvMatrix);   //dohvacanje modelview matrice
	     
	    float posx=0.0, posy=0.0, posz=0.0;
	    if (mvMatrix != NULL){
	      posx = (mvMatrix[12]);                  //izvlacenje koordinati iz MV matrice koje onda saljemo u funkciju za iscrtavanje traga
	      posy = (mvMatrix[13]);
	      posz = (mvMatrix[14]);
	      trail++;
	    }
	      
	  glPopMatrix();
	glPopMatrix(); 
      glPopMatrix();	    
    glPopMatrix(); 
    createTrail(posx, posy, posz);      
    
    glutSwapBuffers();
}



int main(int argc, char **argv)
{
     
    glutInit(&argc, argv); 
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(1368, 766);                               //postavljanje prozora

    glutCreateWindow("Zadatak3 - Marino Mohovic");
    
    glutDisplayFunc(drawScene);
    glutKeyboardFunc(KeyPressed);
    glutTimerFunc(25, update, 0);
    
    initLight();    
    printInst();  
    
    glutMainLoop();

    return 0;
}
