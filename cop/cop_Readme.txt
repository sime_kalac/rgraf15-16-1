Zadatak 1. programiran je u razvojnoj okolini ***KDevelop*** (unutar Linuxa).

-----za pravilno pokretanje programa potreban je eigen -3.2.6 folder koji je korišten
 i tokom laboratorijskih vježbi iz kolegija "Računalna grafika" te pravilno imenovanje vaše .cpp datoteke 
 iz koje pokrećete program u CMakeLists.txt ->   add_executable(FILENAME FILENAME.cpp )
                                                 target_link_libraries(FILENAME ${OPENGL_LIBRARIES} ${GLUT_LIBRARY})


TIPKE:

   [S]    -start
   [X]    -stop
   [Esc]  -kraj



Program započinje uključivanjem knjižnica te inicijalizacijom varijabli i vektora 
koje koristimo u radu, kao što su animacija(varijabla koja nam sluzi kao provjera),
vektori PATHX i PATHY (koristimo ih kako bismo odredili putanju na temelju koordinata x i y),
dist, t i brojac (koristene kod update-a i kretanja po krivuljama, npr. za oznaku završetka
jedne krivulje)...
Izmjenom varijabli, moguće je primjerice podešavati osvjetljenje
i tipke koje se koriste u radu. Izmjenom ostalih varijabli, povećavamo mogućnost da ćemo 
program dovesti u stanje nepravilnog ili pak nikakvog rada. 

Do rješenja sam došao promatrajući vježbu 6. sa predavanja, a iz nje
sam i našao određene dijelove prema kojima sam uredio kod. Prilikom izrade
projekta shvatio sam kako bi možda najlakše bilo napraviti nekoliko Bezierovih
krivulja te ih onda povezati na način da prva završava tamo gdje druga počinje.



OPIS RADA:
---otvaranjem programa prikazuje nam se sfera/lopta koja se nalazi u gornjem, lijevom kutu ekrana te 
podloga koja se prostire od dna do otprilike sredine ekrana
---pritiskom na tipku "s" pokrećemo program odskakivanja lopte po podlozi
---lopta se pokreće sve dok ne pritisnemo tipku "x" ili izađemo iz programa 
pritiskom na tipku "Esc" te za sobom ostavlja trag tamnocrvene boje. Loptu je moguće zaustaviti u bilokojem
trenutku, a isto tako i ponovno pokrenuti nakon toga.
---cijelim putem lopta, koja je prigodno osjenčana i osvjetljena, se rotira kako 
bi prikaz odskakivanja bio što realniji 
---nakon što lopta prođe kroz sve 4 Bezierove krivulje (odskakivanja), ona dolazi 
do donjeg, desnog kuta ekrana (točnije negdje do polovice ekrana, s obzirom da nemože lopta proći kroz podlogu
koja se tamo nalazi) te kojem ostaje u mirnom položaju
 
 
REFERENCE:
	www.stackoverflow.com
	www.youtube.com
	www.opengl.org
	www.gamedev.net
	www.opengl-tutorial.org


IZRADIO: Julian Čop


