#include <GL/gl.h>
#include <GL/glut.h>

#include <iostream>
#include <cmath>
#include <array>

using namespace std;

typedef unsigned char uchar_t;

bool running = false;

/**
 * Definicije svih konstanti
 */
const GLfloat whiteAmbientLight[] = {1.0f, 1.0f, 1.0f};
const GLfloat whiteDiffuseLight[] = {1.0f, 1.0f, 1.0f};
const GLfloat whiteSpecularLight[] = {1.0f, 1.0f, 1.0f};
const GLfloat greenDiffuseMaterial[] = {0.0f, 1.0f, 0.0f};
const GLfloat greenSpecularMaterial[] = {0.0f, 1.0f, 0.0f};
const GLfloat greenAmbientMaterial[] = {0.0f, 1.0f, 0.0f};
const GLfloat mShininess = 10.0f;

const GLfloat red[] = {0.541176f, 0.1098f, 0.1098f, 1.0f};
const GLfloat gray[] = {0.6f, 0.6f, 0.6f, 1.0f};
const GLfloat white[] = {1.0f, 1.0f, 1.0f, 1.0f};
const GLfloat green[] = {0.1f, 0.9f, 0.1f, 1.0f};
const GLfloat blue[] = {0.647f, 0.874f, 0.949f, 1.0f};
const GLfloat black[] = {0.0f, 0.0f, 0.0f, 1.0f};

const int width = 600; // Sirina prozora
const int height = 600; // Visina prozora

/**
 * Koristi se za dobiti binomni koeficijent
 */
long binomials(long n, long k)
{
    long i, b;
    if (0 == k || n == k) return 1;

    if (k > n) return 0;

    if (k > (n - k)) k = n - k;

    if (1 == k) return n;

    b = 1;

    for (i = 1; i <= k; i++) {
        b *= (n - (k - i));

        if (b < 0) return -1; /* ERR!!!! OVERFLOW */

        b /= i;
    }

    return b;
}

/**
 * (1-t)^(n-k) * t^k
 */
double polyterm(const int &n, const int &k, const double &t)
{
    return pow((1.0 - t), n-k) * pow(t, k);
}

/**
 * Vrijednost bezierove krivulje za zadane tocke po x, y i z
 */
double getValue(const double &t, const array<double, 4> &v)
{
    int order = v.size()-1;
    double value = 0;

    for (int n = order, k = 0; k <= n; k++) {
        if (v[k] == 0) continue;
        value += binomials(n, k) * polyterm(n, k, t) * v[k];
    }

    return value;
}

/**
 * Poziva se pri resizeanju prozora
 */
void changeSize(int w, int h)
{
    if (h == 0) h = 1;

    float ratio = 1.0 * w / h; // Sluzi za odrzati omjer

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, w, h);
    gluPerspective(45, ratio, 1, 1000); // view angle u y, aspect, near, far
}

array<double, 4> pt_x, pt_y, pt_z;

/**
 * Sluzi za nacrtati kontrolne tocke pri ispaljivanju kugle
 */
void drawControlPoints()
{
    glEnable(GL_COLOR_MATERIAL); // Ukljucuje se bojanje sa glColor3f(...)
    glPushMatrix();
    glBegin(GL_LINE_STRIP);
    glColor3f(1.0f, 1.0f, 1.0f); // Bijela boja
    glVertex3f(pt_x[0], pt_y[0], pt_z[0]);
    glVertex3f(pt_x[1], pt_y[1], pt_z[1]);
    glVertex3f(pt_x[2], pt_y[2], pt_z[2]);
    glVertex3f(pt_x[3], pt_y[3], pt_z[3]);
    glEnd();
    glPopMatrix();
    glDisable(GL_COLOR_MATERIAL); // Iskljucuje se bojanje sa glColor3f(...)
}

double dist;
double ang = 0;

/**
 * Sfera se crta pomocu parametarske jednadzbe
 */
void drawSphere(double r, int lats, int longs)
{
    for (int i = 0; i <= lats; i++) {
        double lat0 = M_PI * (-0.5 + static_cast<double>((i-1)) / lats);
        double z0 = r * sin(lat0);
        double zr0 = cos(lat0);
 
        double lat1 = M_PI * (-0.5 + static_cast<double>(i) / lats);
        double z1 = r * sin(lat1);
        double zr1 = cos(lat1);
 
 
        glBegin(GL_QUAD_STRIP);
        for (int j = 0; j <= longs; j++) {
            double lng = 2 * M_PI * static_cast<double>((j - 1)) / longs;
            double x = r * cos(lng);
            double y = r * sin(lng);
 
            glNormal3f(-x/r, -y/r, -z0/r);
            /**
             * Postavlja se materijal
             */
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, red);
            glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, red);
            glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white);
            glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, mShininess);
            glVertex3f(x * zr0, y * zr0, z0);
            glNormal3f(-x/r, -y/r, -z1/r);
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, red);
            glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, red);
            glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white);
            glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, mShininess);
            glVertex3f(x * zr1, y * zr1, z1);
        }
        glEnd();
 
    }
}

void light()
{
	glLightfv(GL_LIGHT0, GL_SPECULAR, whiteSpecularLight);
	glLightfv(GL_LIGHT0, GL_AMBIENT, whiteAmbientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDiffuseLight);
    glEnable(GL_LIGHTING); // Ukljucuje se svjetlost
    glEnable(GL_LIGHT0); // LIGHT0
	GLfloat lightPos[] = {0.0f, 0, 1.0f, -2.0f};
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
}

void draw()
{
	glPushMatrix();
        glRotatef(90, 0,1,0); // Od kamere
        glPushMatrix();
            glTranslatef(0.95,0.5,0); // Pocetna pozicija
            glPushMatrix();
				float x, y, z;
				x = getValue(dist, pt_x); // Sljedeca x pozicija
				y = getValue(dist, pt_y); // Sljedeca y pozicija
				z = getValue(dist, pt_z); // Sljedeca z pozicija
//                 cout << x << " " << y << " " << z << endl;
				glPushMatrix();
		 		    glTranslatef(x,y,z); // Translacija na izracunatu poziciju
	 			    glRotatef(ang, 0.0f, 1.0f, 0.0f);
	                drawSphere(0.05, 100, 100); // Crtanje sfere
				glPopMatrix();
            glPopMatrix();
            drawControlPoints(); // Crtanje zadanih tocaka koje sluze za beziera
        glPopMatrix();
    glPopMatrix(); 
}

void drawScene()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor (0, 0, 0, 0.0);
    
    glMatrixMode(GL_MODELVIEW); // idemo u perspektivu
    glLoadIdentity(); // resetiranje

    gluLookAt (0.0,0.0,1.0, // camera
               0.0,0.2,-1.0, // where
               0.0f,1.0f,0.0f); // up vector

	light();

	draw();
   
    glutSwapBuffers();
}

double dist_helper = -90;

void update (int /*value*/)
{
    glutPostRedisplay();
	if (running) {
	    dist_helper += 1;
	    dist = 1 - dist_helper/(-90);
	    if(dist_helper > 0)
            dist_helper = -90;
	}
    //update glut-a nakon 25 ms
    glutTimerFunc(25, update, 0);
}

void handleKeypress(uchar_t key, int /*x*/, int /*y*/)
{
// 	cout << "Pressed " << key << endl;
	switch(key) {
		case 27: // Escape
			exit(0);
			break;
		case 's': // Pokreni animaciju
			cout << "Case s" << endl;
			running = true;
			break;
		case 'e': // Zaustavi animaciju
			cout << "Case e" << endl;
			running = false;
			dist = 0;
			dist_helper = -90;
			break;
	}
}

void handleSpecial(int key, int /*x*/, int /*y*/)
{
    if (!running) {
        switch(key) {
            case GLUT_KEY_UP: // Ako stisnemo strelicu gore
                pt_y[1] += 0.05; // Gornje 2 tocke idu na gore
                if (pt_y[1] > 1) pt_y[1] = 1;
                pt_y[2] = pt_y[1];
                break;
            case GLUT_KEY_DOWN: // Strelica na dolje
                pt_y[1] -= 0.05; // Gornje 2 tocke idu na dolje
                if (pt_y[1] < -1) pt_y[1] = -1;
                pt_y[2] = pt_y[1];
                break;
            case GLUT_KEY_LEFT: // Strelica lijevo
                pt_z[1] -= 0.005; // Micemo 3 tocke koje nisu u ishodistu sa ratiom u lijevo
                pt_z[2] -= 0.01;
                pt_z[3] -= 0.02;
                if (pt_z[3] <= -2) {
                    pt_z[3] = -2;
                    pt_z[2] = -1;
                    pt_z[1] = -0.5;
                }
                break;
            case GLUT_KEY_RIGHT: // Strelica desno
                pt_z[1] += 0.005; // Micemo 3 tocke koje nisu u ishodistu sa ratiom u desno
                pt_z[2] += 0.01;
                pt_z[3] += 0.02;
                if (pt_z[3] >= 2) {
                    pt_z[3] = 2;
                    pt_z[2] = 1;
                    pt_z[1] = 0.5;
                }
                break;
        }
    }
}

void init()
{
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(width, height);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glShadeModel(GL_SMOOTH);
}

void setFunctions()
{
    glutKeyboardFunc(handleKeypress); // Handlamo e i s
    glutReshapeFunc(changeSize); // Resize 
    glutDisplayFunc(drawScene);
    glutTimerFunc(25, update, 0); // Timer funkcija za update
	glutSpecialFunc(handleSpecial); // Handlamo strelice
}

int main(int argc, char **argv)
{
    pt_x = {0, 0, 0 ,0}; // Od mene
    pt_y = {-1, -0.5, -0.5, -1}; // Gore/dolje
    pt_z = {0, 0.25, 0.5, 1}; // Lijevo/desno
    dist = 0.0;

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(width, height);
    init();
    glutCreateWindow("cannonball");
    setFunctions();
    
    glutMainLoop();
    return 0;
}