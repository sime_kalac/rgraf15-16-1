#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <unistd.h>
#include <iostream>
#include <vector>

struct points{ //struktura za koordinate traga
    float x;
    float y;
    float z;
};

float angle1[3]={90,0,0};
float angle2[3]={0,90,0};

int axis1=0;
int axis2=0;

int flag=1;
float worldCoo[16];
std::vector<points> pointsVector; //vektor u koji cemo zapisati sve tocke gdje je bila zadnja sfera za crtanje linije traga

void rotation(){ //funkcija koja rotira tijela na temelju gumba kojeg smo stisnuli
    if (flag) return;
    else{

        if (axis1==1) angle1[0]++;

        else if (axis1==2) angle1[1]++;

        else if (axis1==3)angle1[2]++;

        if (axis2==1) angle2[0]++;

        else if (axis2==2) angle2[1]++;

        else if (axis2==3) angle2[2]++;
    }

    usleep(10000);
    glutPostRedisplay();
}

void drawCube(float x, float y, float z){
    glColor4f(0.5, 0.5, 0.5, 1);

    const GLfloat ambientc[] = {0.25, 0.25, 0.25, 1};
    glMaterialfv(GL_FRONT, GL_AMBIENT, ambientc);
    const GLfloat diffusec[] = {0.4, 0.4, 0.4};
    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffusec);
    const GLfloat specularc[] = {0.774597, 0.774597, 0.774597};
    glMaterialfv(GL_FRONT, GL_SPECULAR, specularc);
    const GLfloat shinynessc= 0.6;
     glMaterialf(GL_FRONT, GL_SHININESS, shinynessc * 128.0);


    glTranslatef(x,y,z);
    glutSolidCube(1.1);
}

void drawCylinder(float kut[], float translate_x, float translate_y, float translate_z) {

    GLUquadricObj *cilinder;
    cilinder = gluNewQuadric();
    gluQuadricDrawStyle(cilinder, GLU_SMOOTH); //nacrta valjak

    const GLfloat ambient1v[] = {0.0, 0.1, 0.6, 1}; //postavke za osvjeljenje i sjecanje
    glMaterialfv(GL_FRONT, GL_AMBIENT, ambient1v);
    const GLfloat diffuse1v[] = {0.1, 0.50980392, 0.50980392};
    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse1v);
    const GLfloat specular1v[] = {0.50196078, 0.50196078, 0.50196078};
    glMaterialfv(GL_FRONT, GL_SPECULAR, specular1v);
    const GLfloat shinyness1v= 0.25;
    glMaterialf(GL_FRONT, GL_SHININESS, shinyness1v * 128.0);

    glRotatef(kut[0], 1, 0, 0); //rotiramo na temelju vrijednosti kuta kojeg posaljemo u funkciju, ovisno o globalnim varijablama angles koje nam se mijenjaju na temelju gumba kojeg smo stisnuli
    glRotatef(kut[1], 0, 1, 0);
    glRotatef(kut[2], 0, 0, 1);
    glTranslatef(translate_x, translate_y, translate_z); //translantiramo na temelju brojeva koje posaljemo u funkciju, jer crtamo iste valjke ali na razlicitim pozicijama
    gluCylinder(cilinder, 0.35f, 0.35f, 2.5f, 24, 6);


}

void drawSphere(double pozicija_x,double pozicija_y, double pozicija_z){
    glColor3f(0.0, 1.0, 0.0);
    const GLfloat ambient1k[] = {0.0, 0.0, 0.0, 1};
    glMaterialfv(GL_FRONT, GL_AMBIENT, ambient1k);
    const GLfloat diffuse1k[] = {0.1, 0.35, 0.1};
    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse1k);
    const GLfloat specular1k[] = {0.45, 0.55, 0.45};
    glMaterialfv(GL_FRONT, GL_SPECULAR, specular1k);
    const GLfloat shinyness1k= 0.25;

    glMaterialf(GL_FRONT, GL_SHININESS, shinyness1k * 128.0);

    glTranslatef(pozicija_x, pozicija_y, pozicija_z); //buduci da crtamo dvije iste kugle, ali na razlicitim pozicijama, ova funkcija prima 3 vrijednosti

    glutSolidSphere(0.5, 20, 20);//crta sferu
}

void drawSpljostenaVeca(float x, float y, float z){


    const GLfloat ambient3k[] = {0.0, 0.0, 0.0, 1};
    glMaterialfv(GL_FRONT, GL_AMBIENT, ambient3k);
    const GLfloat diffuse3k[] = {0.5, 0.5, 0.0};
    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse3k);
    const GLfloat specular3k[] = {0.60, 0.60, 0.50};
    glMaterialfv(GL_FRONT, GL_SPECULAR, specular3k);
    const GLfloat shinyness3k= 0.25;
    glMaterialf(GL_FRONT, GL_SHININESS, shinyness3k * 128.0);


    glColor4f(1, 0.65, 0, 1);
    glTranslatef(x, y, z);
    glScalef(1, 1, 0.5);
    glutSolidSphere(0.5, 20, 20);
}

void display()
{
    glClearColor(0, 0, 0, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();


    glTranslatef(0, 3, -10); //nariktati scenu

    glPushMatrix();
    drawCube(0,0,0);


    //nacrta 1. kuglicu
    glPushMatrix();
    drawSphere(0,-1,0);

    //nacrta 1. valjak
    glPushMatrix();
    glColor3f(0.0, 0.0, 1.0); //valjci razlicitih boja
    drawCylinder(angle1,0,0,0.38);

    //nacrta 2. kuglicu
    glPushMatrix();
    drawSphere(0,0,2.7);

    //nacrta 2. valjak
    glPushMatrix();
    glColor3f(1.0, 0.0, 0.0);
    drawCylinder(angle2,0,0,0.38);


    glPushMatrix();
    drawSpljostenaVeca(0,0,2.7);
    glGetFloatv(GL_MODELVIEW_MATRIX, worldCoo); //iz ove matrice u polje woroldCoo dobijemo, izmedu ostalog, xy i z koordinate centra zadnje nacrtane sfere, koje nam sluze za crtanje traga

    pointsVector.push_back(points());
    pointsVector.back().x = worldCoo[12];
    pointsVector.back().y = worldCoo[13] - 3;
    pointsVector.back().z = worldCoo[14] + 10;

    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
    glPopMatrix();


    glPushMatrix();

    glLineWidth(10);

    glColor3f(1.0,0.0,0.0); //linija crvene boje

    glBegin(GL_LINE_STRIP); //crta linije izmedu svih dodanih tocaka

    for(int i = 1; i < pointsVector.size(); i++){
        glVertex3f(pointsVector[i].x , pointsVector[i].y , pointsVector[i-1].z);
    }

    glEnd();
    glPopMatrix();

    glutSwapBuffers();
    glutPostRedisplay();

}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 81:		//Q,q
    case 113:

        axis1=1;
        break;

    case 87:		//W,w
    case 119:

        axis1=2;
        break;

    case 69:		//E,e
    case 101:

        axis1=3;
        break;

    case 66:		//B,b
    case 98:

        axis2=1;
        break;

    case 78:		//N,n
    case 110:

        axis2=2;
        break;

    case 77:		//M,m
    case 109:

        axis2=3;
        break;

    case 115:		//S,s
    case 83:
        flag=0;
        break;

    case 88:		//X,x
    case 120:
        flag=1;
        break;

    case 27: 		//Esc
        exit(0);
    }

}
void resize(int w, int h)
{
    if (h == 0) { h = 1; }
    float ratio = 1.0* w / h;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,ratio,1,1000); // view angle u y, aspect, near, far
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void init() {
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);  // Osvjetljenje
    glEnable(GL_COLOR_MATERIAL); // Boja materijala
    glEnable(GL_LIGHT0); // Ukljucimo svijetlo0 i pozicioniramo ga na (0.5,0.5,0.5)
    glEnable(GL_BLEND);
    glEnableClientState(GL_VERTEX_ARRAY);
    float pos[] = {0.5, 0.5, 0.5, 1};
    glLightfv(GL_LIGHT0, GL_POSITION, pos);
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(800, 800);

    //otvori prozor
    glutCreateWindow("Kathrin Maeusl, DZ 3");

    init();
    //postavi handler funkcije
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutReshapeFunc(resize);

    glutIdleFunc(rotation); //ponavljaj rotaciju

    glutMainLoop();
    return 0;
}
