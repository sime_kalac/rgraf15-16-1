3. zadatak

3 sfere i 2 cilindra, hijerarhijski se rotiraju se po svim osima ovisno o unosu gumbovima, te za sobom ostavlja trag. Model predstavlja simulaciju dvostrukog klatna.

S,X - Start, Pause
Q,W,E - rotacija oko prve sfere
B,N,E - rotacija oko druge sfere

U main funkciji realizirani su prikaz scene, otvaranje prozora, ukljucivanje unosa tipkovnicom te ponavljanje funkcije rotacije te main funkcije.
Init, koju na pocetku poziva main, ukljucuje atribute poput osvjetljenja, boje materijala, pozicije izvora svjetla.

Realizacija dvostrukog klatna nalazi se u display() funkciji. Ona ukljucuje potrebne atribute, te narikta scenu. Nakon toga se realizira hijerarhijsko stvaranje modela dvostrukog klatna. Najvisi objekt se pusha na pocetku, dok se najnizi pusha na kraju. Kako bi se ostvarila hijerarhija, se tek nakon zadnjeg objekta svi redom poppaju sa stacka. Ono ima za efekt, da se sve transformacije roditelja odvijaju i na child node-ovima. 

U svakoj funkciji crtanja objekta (sfere, cilindra) uneseni su, osim stvaranja samog objekta, i njegova boja, materijal, osvjetljenje i sjencanje (ambient, diffuse, specular i shininess), te poziciju na sceni. Svaka funkcija prima odredene vrijednosti translacije te rotacije. 

Kako bi se rotirale ovisno o unosu gumba, mijenjamo globalne varijable angle1 i angle2. Kada stisnemo odredjeni gumb, odredena varijablja polja angle1/2 se povecava. Vrijednosti tih inkrementiranih polja prilikom svakog stvaranja scene saljemo u funkcije crtanja objekata, koje ih onda rotiraju. 
Obicna globalna varijabla flag, sluzi nam za odvijanje ili stopiranje funkcije rotiranja (sa S ili X na tipkovnici). Ova funkcija se svaki put ponavlja u mainu.

Kako bismo ostvarili trag na zadnjoj sferi, kreirana je struktura koordinatnih tocaka x,y i z ,te vektor po tipu te strukture, koji ce nam sluziti za spremanje koordinata zadnje sfere.
U funkciji display, nakon pushanja zadnje sfere, pomocu Modelview matrice zapisujemo matricu transformacije te sfere u odredeno polje. U tom polju, na 12., 13. i 14. mjestu nalaze se koordinate te sfere s obzirom na scenu ( te smo im oduzeli 3 ili dodali 10, jer smo za toliko manualno namjestali poziciju scene). Njih spremamo u napravljeni vektor.

Poslje poppanja objekata za model penduluma, pusha se linija, koja ne spada u tu hijerarhiju. Ovdje koristim funkciju glBegin(GL_LINE_STRIP), koja crta linije izmedu svih dodanih tocaka. Slijedi for petlja koja ide do velicine vektora, u kojemu se nalaze sve koordinate zadnje sfere. Ona dodaje tocke, po kojoj ce GL_LINE_STRIP crtati linije.
Tu funkciju zavrsavamo sa glEnd, te poppamo tu liniju.

U keyboard funkcji provjeravamo koji je gumb stisnut, te s obzirom na to mijenjamo globalnu varijablu axis/flag, koji nam sluze za odredjivanje osi rotacije u funkciji rotacije ili pokretanja/zaustavlanja animacije.

Kao izvori koristili su vjezbe sa kolegija, ali i:

https://www.opengl.org/sdk/docs/
https://www.opengl.org/discussion_boards/
https://tutorialsplay.com/opengl/
http://devernay.free.fr/cours/opengl/materials.html
http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/
