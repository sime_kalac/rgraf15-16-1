 izvori: 
 -vježbe 4 (tipke), 
 -vježbe 6 (Beziere, smjer, kut ispucavanja)
 -vježbe 7 (template)- lightning
 -https://www.opengl.org/discussion_boards/showthread.php/185186-Blending - iscrtavanje kugle
 -(lightnig position):
 -https://www.cse.msu.edu/~cse872/tutorial3.html
 
 -xoax.net tutorijali
 -http://www.theasciicode.com.ar/ 

Program pokazuje čeličnu kuglu i bezierovu krivulju. Pritiskom na tipku "s" (također i "S" ) kugla se počinje kretati po krivulji i 
kreće se sve dok ne dostigne njenu krajnju točku (dist==1). Tipka "e" pauzira animaciju, tipka "r" vraća kuglu na početak. 
Tipkama "2", "3", "4", odabiremo točku čije točke x ili y želimo promijeniti, dok nakon odabira tipkama "+", "-" mijenjamo y, a "l","k"
mijenjamo x. Os z svih točaka je na 0, a smjer mijenjamo rotiranjem kompletne scene- tipkama "x" i "y" u CW i CCW smjeru. 
Tipkama "c" i "b" uključuje se i isključuje iscrtavanje stošca koji pokazuje u smjeru u kojem će kugla krenuti, te bezierove krivulje(zajedno s kontrolnim točkama)

Update funkcija svakih 25 ms ponovno pozove drawScene gdje se na temelju točaka iznova generira bezierova krivulja i kugla
se iscrtava na novoj poziciji koja je definirana pozicijama točaka i smjerom.

Funkcija koja iscrtava kuglu koristi kao ulazne argumente broj vertikalnih i horizontalnih podjela te GL_QUAD_STRIP. Koristi knjižnicu math.h
Ovaj kod također koristi i eigen 3.2.6.
 
