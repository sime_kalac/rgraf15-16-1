#include <GL/glut.h>
#include <math.h>
#include <GL/gl.h>
#include <cmath>
#include <iostream>
#include <Eigen/Dense>
#include <string.h>
#define X .525731112119133606 
#define Z .850650808352039932
using namespace Eigen;
using namespace std;

float Px1=0,Py1=0,Px2=2,Py2=2,Px3=6,Py3=3,Px4=9,Py4=0;

Vector4d pt_x, pt_y, pt_z;
VectorXd t_x, t_y, t_z;
VectorXd ts;
Vector3d sphere_coo;

//osvjetljenje
GLfloat redDiffuseMaterial[] = {1.0, 0.0, 0.0}; //set the material to red
GLfloat whiteSpecularMaterial[] = {1.0, 1.0, 1.0}; //set the material to white
GLfloat greenEmissiveMaterial[] = {0.0, 1.0, 0.0}; //set the material to green
GLfloat whiteSpecularLight[] = {1.0, 1.0, 1.0}; //set the light specular to white
GLfloat blackAmbientLight[] = {0.0, 0.0, 0.0}; //set the  light ambient to black
GLfloat redAmbientLight[] = {1.0, 0.0, 0.0}; //set the  light ambient to red
GLfloat whiteAmbientLight[] = {1.0, 1.0, 1.0}; //set the  light ambient to white
GLfloat whiteDiffuseLight[] = {1.0, 1.0, 1.0}; //set the  diffuse red to white
GLfloat redDiffuseLight[] = {1.0, 0.1, 0.1}; //set the  diffuse red to white
GLfloat blankMaterial[] = {0.0, 0.0, 0.0}; //set the diffuse  light to white
GLfloat mShininess[] = {128}; //set the shininess of the material

GLfloat qaLightPosition[]    = {1, 10, 0, 0}; //x,y,z direction

//kretanje
float smjer = 0;
float dist= 0; 
//tipke
bool keyStates[256];
bool on=0;

class Point {
public:
    float x, y;
    void setxy(float x2, float y2) { x = x2; y = y2; }
    const Point & operator=(const Point &rPoint) {
         x = rPoint.x;
         y = rPoint.y;        
         return *this;
      }

};

class Odabir {
public:
  int q;
};


static GLfloat vdata[12][3] = {    
    {-X, 0.0, Z}, {X, 0.0, Z}, {-X, 0.0, -Z}, {X, 0.0, -Z},    
    {0.0, Z, X}, {0.0, Z, -X}, {0.0, -Z, X}, {0.0, -Z, -X},    
    {Z, X, 0.0}, {-Z, X, 0.0}, {Z, -X, 0.0}, {-Z, -X, 0.0} 
};
static GLuint tindices[20][3] = { 
    {0,4,1}, {0,9,4}, {9,5,4}, {4,5,8}, {4,8,1},    
    {8,10,1}, {8,3,10}, {5,3,8}, {5,2,3}, {2,7,3},    
    {7,10,3}, {7,6,10}, {7,11,6}, {11,0,6}, {0,1,6}, 
    {6,1,10}, {9,0,11}, {9,11,2}, {9,2,5}, {7,2,11} };

void normalize(GLfloat *a) {
    GLfloat d=sqrt(a[0]*a[0]+a[1]*a[1]+a[2]*a[2]);
    a[0]/=d; a[1]/=d; a[2]/=d;
}

void drawtri(GLfloat *a, GLfloat *b, GLfloat *c, int div, float r) {
    if (div<=0) {
        glNormal3fv(a); glVertex3f(a[0]*r, a[1]*r, a[2]*r);
        glNormal3fv(b); glVertex3f(b[0]*r, b[1]*r, b[2]*r);
        glNormal3fv(c); glVertex3f(c[0]*r, c[1]*r, c[2]*r);
    } else {
        GLfloat ab[3], ac[3], bc[3];
        for (int i=0;i<3;i++) {
            ab[i]=(a[i]+b[i])/2;
            ac[i]=(a[i]+c[i])/2;
            bc[i]=(b[i]+c[i])/2;
        }
        normalize(ab); normalize(ac); normalize(bc);
        drawtri(a, ab, ac, div-1, r);
        drawtri(b, bc, ab, div-1, r);
        drawtri(c, ac, bc, div-1, r);
        drawtri(ab, bc, ac, div-1, r);
    }  
}
void changeSize ( int w, int h )
{
    if ( h == 0 )
    {
        h = 1;
    }

    float ratio = 1.0* w / h;
    glMatrixMode ( GL_PROJECTION );
    glLoadIdentity();
    glViewport ( 0, 0, w, h );
    gluPerspective ( 45,ratio,1,1000 ); // view angle u y, aspect, near, far
}

void drawsphere(int ndiv, float radius=1.0) {
    glBegin(GL_TRIANGLES);
    for (int i=0;i<20;i++)
        drawtri(vdata[tindices[i][0]], vdata[tindices[i][1]], vdata[tindices[i][2]], ndiv, radius);
    glEnd();
}
Point abc[4];

void drawCone()
{
    glPushMatrix();
    glColor3f ( 0.0f, 1.0f, 0.0f );
    glutWireCone(0.2, 0.8, 16,4);
    glPopMatrix();
}
void myInit() {
    glClearColor(0.0,0.0,0.0,0.0);
    glColor3f(1.0,0.0,0.0);
    glPointSize(8.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0,64.0,0.0,48.0);
}

void drawDot(int x, int y) {
    glBegin(GL_POINTS);
    glVertex2i(x,y);
    glEnd();
    glFlush();
}

void drawLine(Point p1, Point p2) {
    glBegin(GL_LINES);
    glVertex2f(p1.x, p1.y);
    glVertex2f(p2.x, p2.y);
    glEnd();
    glFlush();
}

// Calculate the next bezier point.
Point drawBezier(Point A, Point B, Point C, Point D, double t) {
    Point P;
    P.x = pow((1 - t), 3) * A.x + 3 * t * pow((1 -t), 2) * B.x + 3 * (1-t) * pow(t, 2)* C.x + pow (t, 3)* D.x;
    P.y = pow((1 - t), 3) * A.y + 3 * t * pow((1 -t), 2) * B.y + 3 * (1-t) * pow(t, 2)* C.y + pow (t, 3)* D.y;      
    return P;
}


void init (void) {
    glEnable (GL_DEPTH_TEST);
    glEnable(GL_SMOOTH);
    glEnable (GL_LIGHTING);
    glEnable (GL_LIGHT0);
    
    //materijal
}

void light (void) {
    glLightfv(GL_LIGHT0, GL_SPECULAR, whiteSpecularLight);
    glLightfv(GL_LIGHT0, GL_AMBIENT, blackAmbientLight);
    glLightfv(GL_LIGHT0, GL_AMBIENT, whiteAmbientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDiffuseLight);
    glLightfv(GL_LIGHT0, GL_POSITION, qaLightPosition);
}

void keyOperations (unsigned char menu) 
{
 // putanja ograničena na "optimalne vrijednosti" radi urednosti i prirodnog leta kugle
  if(keyStates[27]){ // esc
   
      exit(0);
    }   
    
     if((menu=='s')||(menu=='S')){ 
      on=1;
    } else if((menu=='r')||(menu=='R')){
       dist=0;
    }if((menu=='e')||(menu=='E')){
      on=0;
     }
     
   switch(menu){  //caps OFF
     case 't' : Py2++;break;
     case 'g' : if(Py2<=2)
		  {Py2=2;};Py2--;break;
     case 'f' : if(Px2<=1)
		  {Px2=1;};Px2--;break;
     case 'h' : Px2++;break;
     
     
     case 'j' : if(Px3<=5)
		  { Px3=5;};Px3--;break;
     case 'l' : Px3++;break;
     case 'k' : if(Py3<=3)
		  { Py3=3;};Py3--;break;
     case 'i' : Py3++;break;
     
     
     case 'o' : if(Px4<=9)
		  {Px4=9; };Px4--;break;
     case 'p' : Px4++;break;
    

  }
 
    
}
void keyPressed (unsigned char key, int /*x*/, int /*y*/) 
{
    keyStates[key] = true; 
    keyOperations(key);
    
}
void keyUp (unsigned char key, int /*x*/, int /*y*/) 
{
    keyStates[key] = false;
}

long binomials ( long n, long k )
{
    // n && k >=0
    long i;
    long b;

    if ( 0 == k || n == k )
    {
        return 1;
    }

    if ( k > n )
    {
        return 0;
    }

    if ( k > ( n - k ) )
    {
        k = n - k;
    }

    if ( 1 == k )
    {
        return n;
    }

    b = 1;

    for ( i = 1; i <= k; ++i )
    {
        b *= ( n - ( k - i ) );

        if ( b < 0 ) return -1; /* ERR!!!! OVERFLOW */

        b /= i;
    }

    return b;
}

double polyterm ( const int &n, const int &k, const double &t )
{
    return pow ( ( 1.-t ),n-k ) *pow ( t,k );
}

double getValue ( const double &t, const VectorXd &v )
{
    int order = v.size()-1;
    double value = 0;

    for ( int n=order, k=0; k<=n; k++ )
    {
        if ( v ( k ) ==0 ) continue;

        value += binomials ( n,k ) * polyterm ( n,k,t ) * v ( k );
    }

    return value;
}

void bezier(){
  	 
  glClear(GL_COLOR_BUFFER_BIT);   
    
Point POld=abc[0];
    
    //Promjena točaka sa tipkama preko PxX
    abc[0].x=0,abc[0].y=0;
    abc[1].x=Px2,abc[1].y=Py2;
    abc[2].x=Px3,abc[2].y=Py3;
    abc[3].x=Px4,abc[3].y=Py4;
    
    glColor3f(1.0,0.0,0.0);
    
    for(int i=0;i<4;i++)
        //drawDot(abc[i].x, abc[i].y);       
    glColor3f(1.0,1.0,1.0);
    
    drawLine(abc[0], abc[1]);
    drawLine(abc[1], abc[2]);
    drawLine(abc[2], abc[3]);  
  
    for(double t = 0.0;t <= 1.0; t += 0.1) {
        Point P = drawBezier(abc[0], abc[1], abc[2], abc[3],  t);
        drawLine(POld, P);  // ne vidi se linija po kojoj ide
	
        POld = P;       
    }
  
    glFlush();
}

void drawScene()
{
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glClearColor (0.3, 0.3, 0.3, 0.0);
    
    glMatrixMode ( GL_MODELVIEW ); // idemo u perspektivu
    glLoadIdentity(); // resetiranje   
    float x,y,z;
    
    x = getValue(dist, pt_x);
    y = getValue(dist, pt_y);
    z = getValue(dist, pt_z);
    
    gluLookAt ( 0.0,20.0,10.0, // camera
                0.0,0.0,-1.0, // where
                0.0f,1.0f,0.0f ); // up vector

    glPushMatrix();
        glRotatef(smjer, 0,1,0);
        drawCone();
            glPushMatrix();

	    
   pt_x<< abc[0].x, abc[1].x,abc[2].x,abc[3].x;
   pt_y<< abc[0].y,abc[1].y,abc[2].y,abc[3].y;

	bezier();
	      glPushMatrix();
		glTranslatef(x,y,z); 
		  glPushMatrix();
		    light();
		    glColor3f(0.5,0.5,0.5);
		    glScalef(0.5,0.5,0.5);
		    //solidSphere(2,40,40);
		    drawsphere(6,1);
		  glPopMatrix();
		  
		glPopMatrix();   
	    glPopMatrix();
       glPopMatrix();
    glPopMatrix();
   
    glutSwapBuffers();
}

float t = 100; 
int loop = 1;

void update ( int /*value*/ )
{  
    glutPostRedisplay();
    if(on){
    //dist = 1 - cone_angle/(0-90);
    if(dist<1){ 
    dist+=00.05;}
      
    if(smjer > 280 ){smjer = -90;}
    //update glut-a nakon 25 ms
    if (dist>1){ on=0;}; 
    }
    glutTimerFunc ( t, update, 0 );

}


void special(int k, int /*x*/,int /*y*/) {
       switch(k) {
       case GLUT_KEY_LEFT:    smjer += 1; break;
       case GLUT_KEY_RIGHT:  smjer -=1; break;
     
       }
    //   computeLocation();
       glutPostRedisplay();
   }
   

int main ( int argc, char **argv )
{
     for(int i=0; i<256; ++i)
    { keyStates[i] = false; }


    sphere_coo << pt_x(0), pt_y(0), pt_z(0);
    glutInit ( &argc, argv );
    
    glutInitDisplayMode ( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize ( 1000, 500 );
    glutCreateWindow (""); 
    glutReshapeFunc ( changeSize );
    init();
    glutDisplayFunc ( drawScene );
   
    glutTimerFunc ( 25, update, 0 );
    glShadeModel (GL_SMOOTH);
       
    glutKeyboardFunc(keyPressed); 
    glutKeyboardUpFunc(keyUp); 
    glutSpecialFunc(special);
    glutMainLoop();
    
    return 0;
}


