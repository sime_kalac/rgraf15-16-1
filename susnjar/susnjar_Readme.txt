4. zadatak, Lansiranje kugle
*za rad programa je potrebno dodati eigen-3.2.6.
Lansiranje kugle je  "ograničeno gravitacijom", tako da se kugla ne može početi vraćati u suprotnom smjeru, također, krajnja točka je ograničena na minimalnu udaljenost.
Svaki trokut na ikosaedru se dijeli na manje i tako se iz 12 vrhova i 30 bridova postupno dobije
fino izglađena kugla. Bezierova krivulja je određena s 4 točke koje imaju svoju početnu vrijednost, ali i koje pomičemo zbog željene promjene putanje kugle.

S - pokreće "paljbu"
R - vraća na početno stanje
E - pauziraj
ESC - izlaz
<- - strijelica lijevo za rotaciju u smjeru kazaljke na satu
-> - strijelica desno za rotaciju suprotnu smjeru kazaljke na satu
(tipke za pomak su ovisne o caps locku)
 pomak TOČKA 2:
_____________________
      T-gore
F-lijevo     H-desno
      G-dolje  
_____________________  		
       TOČKA 3:
      I-gore
J-lijevo     L-desno
      K-dolje
_____________________
       TOČKA 4:  
O-lijevo     P-desno
_____________________

reference:
-Laboratorijske vježbe
-opengl.org.ru
-wikipedia
