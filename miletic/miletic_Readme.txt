
Zadatak 3

Korištenje programa:

Za korištenje ovog programa potrebno je koristiti tipke q, w, e za rotaciju gornjg klatna te b, n, m za rotaciju donjg klatna. Pritiskom na ecs tipku izazi se iz programa, a pritiskom na 's' program se pokreće.

Nakon što se animacija pokrene pritiskom na tipku 's' za korištenje potrebne su vam ove tipke:
- 'q' – omogućava rotaciju prvog klatna oko Z osi
- 'w' – omogućava rotaciju prvog klatna oko Y osi
- 'e' – omogućava rotaciju prvog klatna oko X osi
- 'b' – omogućava rotaciju drugog klatna oko Z osi
- 'm' - omogućava rotaciju drugog klatna oko X osi
- 'n' -  omogućava rotaciju drugog klatna oko Y osi
- 'x' -  omogućava pauziranje animacije sve dok se ponovno ne pritisne tipka 's'.

Stvaranje programa:
Za stvaranje dvostrukog klatna obješenog za fiksnu točku bilo je potrebno koristiti hijerarhijsko modeliranje koje je objašnjeno u predavanju p05 te na 5.vježbama.
U mome zadatku fiksnu točku predstavlja kocka nacrtana pomoću funkcije void glutSolidCube(GLdouble size)(1) koja generira kocu određene veličine, (u mom primjeru 0.2f).
Kugle oko kojih se klatna rotiraju nacrtane su pomoću funkcije glutSolidSphere(GLdouble radius, GLint slices, GLint stacks)(2) za koju sam prethodno odredila radijus te argumente 'slices' i 'stacks' radi kojih kugla izgleda zaobljenije.

Za početak je bilo potrebno napraviti translaciju prve kugle kako bi ju postavila na određeno mjesto.   Zatim sam stvorila prvo klatno koje sam pomoću funkcija rotacije i translacije postavila točno ispod napravljene kugle. Za crtanje klatna koristila sam funkciju koja je potrebna kada crtamo kvadratne oblike: 
GLUquadricObj *object;(5)
object = gluNewQuadric();
gluCylinder(object,0.03f,0.03f,0.46f,100,100);      

Isti postupak vrijedi za ostale dvije kugle i klatno. Vrijednosti za rotaciju i translaciju klatna sam odabirala prema vrijednostima koje sam odredila za pozicioniranje prve kugle.
Svaki dio napravljenog lika nalazi se unutar funkcija pushMatrix i popMatrix.
Kada je lik bio gotov dodala sam fiksnu točku(kocku) koju sam translacijom dovela na određenu poziciju iznad prve kugle.
Cratanje dvostrukog klatna odvija se u funkcijma drawFigure(), sphere_line(), rotate1() I rotate2() koje su odvojene radi lakšeg snalaženja u kodu.

Rotacija klatna oko određene osi ovisi o kutu čija se vrijednost mijenja u funkciji update_angle().
Kada vrijednost kuta dođe do 360 stupnjeva kut se postavlja u 0.
Ako se primjerice odvija rotacija oko osi x, potrebno je tu poziciju u funkciji postaviti na 1 : glRotatef(kut1, 1.f, 0.f, 0.f ); i tako zasebno za svaku os.


Za kraj je bilo potrebno napraviti da kugla na donjem klatnu ispušta crveni trag što sam postigla funkcijom  glGetFloatv(GL_MODELVIEW_MATRIX, &array1[counter][0]);(3)
Funkcija dohvaća vrijednosti polja u kojeg se spremjau koordinate translatirane kugle.
Funkcija drawLine() ispisuje određene točke koje se spajaju u liniju pomoću  glBegin(GL_LINE_STRIP);4.
Točke, odnosno linije se ispisuju sve dok counter ne dođe do zadane vrijednosti 150, nakon toga se linija ponovno ispisuje s čime je postignut efekt brisanja linije. Kako se linija nebi iscrtavala čim pritisnem tipku 's' uvela sam flag koji je u tom slučaju false i ispis linije se nece dogoditi.
Osvetljenje i sjenčanje određeno je unutar funkcije lightening(). 
Odabir boja nije određen u zaatku pa sam proizvoljno klatna obojala u zelenu a kugle u žutu boju pomoću glColor3f(1.0f, 1.0f, 0.0f); (primjer žute boje).

Korišteni izvori s interneta:

1  https://www.opengl.org/documentation/specs/glut/spec3/node82.html
2 https://www.opengl.org/resources/libraries/glut/spec3/node81.html
3  https://www.opengl.org/discussion_boards/showthread.php/127175-glGetFloatv%28GL_MODELVIEW_MATRIX-m%29
3 https://www.opengl.org/sdk/docs/man2/xhtml/glGet.xml
3 http://www.gamedev.net/topic/495555-help--with-gl_modelview_matrix/
4 https://www.opengl.org/sdk/docs/man2/xhtml/glBegin.xml
5 http://opengl.czweb.org/ch13/452-454.html
5 https://forum.qt.io/topic/17063/cylinder-with-glucylinder
