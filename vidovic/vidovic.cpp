#include <iostream>
#include <GL/gl.h>
#include <GL/glut.h>
#include <math.h>

GLfloat diffuseMaterial[] = {0.7, 0.7, 0.7};
GLfloat specularMaterial[] = {1.0, 1.0, 1.0};
GLfloat specularLight[] = {1.0, 1.0, 1.0};
GLfloat ambientLight[] = {1.0, 1.0, 1.0, 1.0};
GLfloat diffuseLight[] = {1.0, 1.0, 1.0, 1.0};
GLfloat lightPosition[] = {3.0, 0.0, 3.0, 1.0};
GLfloat shininess[] = {50};

float beziertoc[4][3] = {
	{-2,-2, 4},
	{ 0, 5, 1},
	{ 2, 3,-1},
	{ 4,-2,-4}
};
float index[20][3] = {
	{ 0, 4, 1},{ 0, 9, 4},
	{ 9, 5, 4},{ 4, 5, 8},
	{ 4, 8, 1},{ 8,10, 1},
	{ 8, 3,10},{ 5, 3, 8},
	{ 5, 2, 3},{ 2, 7, 3},
	{ 7,10, 3},{ 7, 6,10},
	{ 7,11, 6},{11, 0, 6},
	{ 0, 1, 6},{ 6, 1,10},
	{ 9, 0,11},{ 9,11, 2},
	{ 9, 2, 5},{ 7, 2,11}
};
float vertices[12][3] = {
	{ -0.5,  0.0, 0.85},{  0.5,  0.0, 0.85},
	{ -0.5,  0.0,-0.85},{  0.5,  0.0,-0.85},
	{  0.0, 0.85,  0.5},{  0.0, 0.85, -0.5},
	{  0.0,-0.85,  0.5},{  0.0,-0.85, -0.5},
	{ 0.85,  0.5,  0.0},{-0.85,  0.5,  0.0},
	{ 0.85, -0.5,  0.0},{-0.85, -0.5,  0.0}
};

int rotation = 0;
float tval = 0.0;
float increment = 0.0;

void initScene();
void display();
void animate();
void drawTravelPath();
void drawSphere();
void measure(float, float *);
void compute(float *, float *, float, float *);
void split(int, float *, float *, float *);
void halfAndNorm(float *,float *,float *);
void reshape(int, int);
void keys(unsigned char, int, int);
void scale(float);

int main(int argc, char **argv){
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowPosition(0, 0);
    glutInitWindowSize(800, 600);
    glutCreateWindow("Ilija Vidovic");
    glutDisplayFunc(display);
    glutIdleFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keys);  
    initScene();
    glutMainLoop();
    return 0;
}

void initScene(){
	glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.02);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specularMaterial);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuseMaterial);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	glShadeModel(GL_SMOOTH);
}

void display(){
    glClearColor(0.1,0.1,0.1,1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    animate();
    glutSwapBuffers();
}

void animate(){
	glTranslatef(0,0,-8);
	glRotatef(rotation, 0,0,1);
	drawTravelPath();
	float p[3];
	measure(tval,p);
	glTranslatef(p[0],p[1],p[2]);
	drawSphere();
	tval=tval+increment;
	if(tval > 1.0) tval = 1.0;
}

void drawTravelPath(){
	glColor4f(0,0.6,1,1);
	glDisable(GL_LIGHTING);
	glBegin(GL_LINE_STRIP);
	float p[3];
	for(float i=0.0;i<=1.0;i+=0.01){
		measure(i,p);
		glVertex3fv(p);
	};
	glEnd();
	glEnable(GL_LIGHTING);
}

void drawSphere(){
	glBegin(GL_TRIANGLES);
	for(int i=0;i<20;i++){
		split(0, vertices[(int)index[i][0]], vertices[(int)index[i][1]], vertices[(int)index[i][2]]);
	};
	glEnd();
}

void measure(float t, float *p){
	float m01[3],m012[3],m12[3],m123[3],m23[3];
	compute(beziertoc[0], beziertoc[1], t, m01);
	compute(beziertoc[1], beziertoc[2], t, m12);
	compute(beziertoc[2], beziertoc[3], t, m23);
	compute(m01, m12, t, m012);
	compute(m12, m23, t, m123);
	compute(m012, m123, t, p);
}

void compute(float *a, float *b, float i, float *retval){
	retval[0]=(1.0-i)*a[0]+i*b[0];
	retval[1]=(1.0-i)*a[1]+i*b[1];
	retval[2]=(1.0-i)*a[2]+i*b[2];
}

void split(int depth, float *a, float *b, float *c){
	depth++;
	if(depth==4){
		glNormal3fv(a); glVertex3fv(a); //a
		glNormal3fv(b); glVertex3fv(b); //b
		glNormal3fv(c); glVertex3fv(c); //c
	}else{
		float ab[3],ac[3],bc[3];
		halfAndNorm(a,b,ab); 
		halfAndNorm(a,c,ac); 
		halfAndNorm(b,c,bc); 
		split(depth,ab,bc,ac);
		split(depth,a,ab,ac);
		split(depth,bc,c,ac);
		split(depth,ab,b,bc);
	};
}

void halfAndNorm(float *a, float *b, float *retval){
	float norm = sqrt(
		pow(a[0]/2+b[0]/2,2) + 
		pow(a[1]/2+b[1]/2,2) + 
		pow(a[2]/2+b[2]/2,2) 
	);
	retval[0]=(a[0]/2+b[0]/2)/norm; 
	retval[1]=(a[1]/2+b[1]/2)/norm; 
	retval[2]=(a[2]/2+b[2]/2)/norm;
}

void reshape(int w, int h){
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, w, h);
    gluPerspective(50, (float)w/h, 1.0, 100.0);
    glMatrixMode(GL_MODELVIEW);
}

void keys(unsigned char key, int, int){
	switch(key){
		case 's':{
			increment=0.0001;
			break;
		}
		case 'e':{
			increment=0.0;
			break;
		}
		case 27:{
			exit(1);
			break;
		}
		case 'd':{
			rotation++;
			break;
		}
		case 'g':{
			rotation--;
			break;
		}
		case 'r':{
			scale(1.1);
			break;
		}
		case 'f':{
			scale(0.9);
			break;
		}
	};
}

void scale(float k){
	for(int i=0;i<4;i++) beziertoc[i][1]*=k;
}
